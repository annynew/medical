var medTermsApp = angular.module('medTermsApp', []);

// Define the controller
medTermsApp.controller('MedTermsController', function MedTermsController($scope, $window) {
  $scope.currentPage = 1;	
  
   /*
    * load medical terms. in a regular app there should be a service call that loads json. 
    * It will return one page at a time and service call will be something like this:
    * 
    * getMedicalTerms/<pageNum>/<nOfRecords>
    * for sorting we could pass:
    * getMedicalTerms/1/5/sort/patNum_asc   meaning sort by patient number in ascending order or
    * getMedicalTerms/1/5/sort/term_desc    meaning sort by term names alphabetically in descending order
    */

  $scope.medTermsContent = {
		"currentPage":1,
		"nOfPages":3,
		"nOfRecords":8,
		"sortBy" : "termTitle",
		"order": "asc",
		"medicalTerms": [ 
            { "termId":28,"termTitle":"ADHD", "nOfPatients":422 },
			{ "termId":22,"termTitle":"Allergies", "nOfPatients":42 },
			{ "termId":27,"termTitle":"Asthma", "nOfPatients":422 },
			{ "termId":24,"termTitle":"Diabetes", "nOfPatients":422 },
			{ "termId":26,"termTitle":"DVT", "nOfPatients":422 },
			{ "termId":29,"termTitle":"Epilepsy", "nOfPatients":422 },
			{ "termId":23,"termTitle":"Flu", "nOfPatients":422 },
			{ "termId":25,"termTitle":"Hepatitis C", "nOfPatients":422 }
		]
  };
  
  /*
   * convert to array so we can iterate over number of pages when displaying them
   * */
  $scope.countPages = function(pages){
	 return new Array(pages); 
  };
  

  /*
   * return "active" as class name for a pagenumber
   * */
  $scope.isActive = function(pageNumber) {
	  if($scope.currentPage  == pageNumber) {
		  return "active";
	  } else {
		  return "";
	  }
  };
  
  /*
   * click on page number
   * */
  $scope.pageClick = function(pageNum) {
	  $scope.currentPage = pageNum + 1; 
  };
  
  /*
   * click on previous link
   * */
  $scope.prevClick = function() {
	  if( $scope.currentPage >1) {
		  $scope.currentPage--; 
	  }
  };
  
  /*
   * click on next link
   * */
  $scope.nextClick = function() {
	  if($scope.currentPage < $scope.medTermsContent.nOfPages) {
			  $scope.currentPage++; 
  	  }
  };
  
  /*
   * open reference url in a new tab
   * */
  $scope.openLink = function(url) {
	  $window.open(url, "_blank");
  };
  
  
  /*
   * this will call rest service for the clicked medical term, e.g.
   *  /getTermReferences/<termId> as in /getTermReferences/22
   *  
   *  termTitle will come from back-end eventually, for this excercise just passing the term from UI so we can search dynamically
   * */
  $scope.loadReferences = function(termId, termTitle) {
	  $scope.referencesContent = 
           	{"termId": 11, "medicalTerm":termTitle, 
			 "links":[
			          {"linkId":33, "displayText":"Search PubMed", "url":"http://www.ncbi.nlm.nih.gov/pubmed?term=" + termTitle },
			          {"linkId":35, "displayText":"Search Google", "url":"http://www.google.com/search?q=" + termTitle }
                  	]
              };
  }
});