#Medical terms project
This is a sample project loading medical terms using angular.js and bootstrap frameworks.

#User story

* As a physician I’d like to see list of medical terms with patient counts, as in:

     [Flu (4200)]()

     [Allergies (22)]()


* I would like to click on medical term and open a pop-up window which has links associated with the selected term. So for example if Flu is clicked, popup will open that displays associated links:

     [Google Flu link](http://www.google.com/search?q=flu) 

     [Pubmed link](http://www.ncbi.nlm.nih.gov/pubmed?term=flu)
 
* When displaying medical terms, I’d like to be able load large number of terms, and possibly paginate through.
* I’d like to be able to sort either by the term title or number of patients associated with it.

## Assumptions:
* We can expect counts of patients associated with any term to be high, however they don’t need to be real time, i.e. counts could be done on a regular basis overnight or once a week
* We can assume that loading of terms and links is done in another story, so data are available to be queried
* We can assume that we know service calls that will return medical terms and associated links for each term

#Data Contract definitions
I would want to have json data loaded as medical terms with patient counts on page load.

Json sample can be:


```json
    {
		"currentPage":1,
		"nOfPages":3,
		"nOfRecords":8,
		"sortBy" : "termTitle",
		"order": "asc",
		"medicalTerms": [ 
            { "termId":28,"termTitle":"ADHD", "nOfPatients":422 },
			{ "termId":22,"termTitle":"Allergies", "nOfPatients":42 },
			{ "termId":27,"termTitle":"Asthma", "nOfPatients":422 },
			{ "termId":24,"termTitle":"Diabetes", "nOfPatients":422 },
			{ "termId":26,"termTitle":"DVT", "nOfPatients":422 },
			{ "termId":29,"termTitle":"Epilepsy", "nOfPatients":422 },
			{ "termId":23,"termTitle":"Flu", "nOfPatients":422 },
			{ "termId":25,"termTitle":"Hepatitis C", "nOfPatients":422 }
		]
  }
```

##Service end-points
To request medical terms we’ll pass page number and number of requested records: 

`/getMedicalTerms/<pageNum>/<nOfRecords>` 

e.g. to get first page and 5 records we’ll have API call:

`/getMedicalTerms/1/5` 

We should also define how returned records are sorted, they can be for example sorted by **term title**, or by a **number of patients** associated with each. For example to sort by **patient number in ascending order**: 


`/getMedicalTerms/1/5/sort/patNum_asc` 

and to sort **by terms in descending order**: 

`/getMedicalTerms/1/5/sort/term_desc`  


#UI display approach

* We’ll loop through received medical terms data and display them. 
* Each term could have many references associated with it, and back-end might contain some sort of reference table that stores that relationship:
* So when we click on a term, service call will pass termId and service will return all associated links. So for example service call can be:

`/getTermReferences/<termId>`

and json returned will be: 

```json
{"termId": 11, "medicalTerm":termTitle, 
			 "links":[
			          {"linkId":33, "displayText":"Search PubMed", "url":"http://www.ncbi.nlm.nih.gov/pubmed?term=flu" },
			          {"linkId":35, "displayText":"Search Google", "url":"http://www.google.com?q=flu" }
                  	]
              };
```


There could another option where we have predefined search engines (as google, etc..) and known ways build a query string. So when term is clicked, rest service can return list of predefined engines and url can either be built by UI or fully by service and returned as list of links for the selected term.

#Project file structure and flow
* `app.js` in `js` folder defines `medTermsAp` app and `MedTermsController` controller.  
* when index.html is loaded, it initializes controller, which will issue a call to a service to retrieve medical terms. In our case I just initialize `$scope.medTermsContent` model object with json data corresponding to medical terms. 
* returned json will contain information on number of pages, number of records, sorting and order applied. 

```json
    {
		"currentPage":1,
		"nOfPages":3,
		"nOfRecords":8,
		"sortBy" : "termTitle",
		"order": "asc",
		"medicalTerms": [ 
            { "termId":28,"termTitle":"ADHD", "nOfPatients":422 },
			{ "termId":22,"termTitle":"Allergies", "nOfPatients":42 },
			{ "termId":27,"termTitle":"Asthma", "nOfPatients":422 },
			{ "termId":24,"termTitle":"Diabetes", "nOfPatients":422 },
			{ "termId":26,"termTitle":"DVT", "nOfPatients":422 },
			{ "termId":29,"termTitle":"Epilepsy", "nOfPatients":422 },
			{ "termId":23,"termTitle":"Flu", "nOfPatients":422 },
			{ "termId":25,"termTitle":"Hepatitis C", "nOfPatients":422 }
		]
  }
```
 
* each medical term is displayed as a link and when clicked, `loadReferences(termId, termTitle)` function is called which opens modal dialog with reference links. At this point service call to `/getTermReferences/<termId>` will be issued, and json with related links will be returned by service and loaded in a modal dialog

```json
{"termId": 11, "medicalTerm":termTitle, 
			 "links":[
			          {"linkId":33, "displayText":"Search PubMed", "url":"http://www.ncbi.nlm.nih.gov/pubmed?term=" + termTitle },
			          {"linkId":35, "displayText":"Search Google", "url":"http://www.google.com?q=" + termTitle }
                  	]
              };
```

* in our case each link appends term to the query string, and when link is clicked `openLink()` function is called which opens new tab and loads url in it for the queried term.
* `bower_components` folder contains angular and bootstrap files
* `css` folder contains custom.css with few minor style additions


#Other Considerations
+ I’m not sure showing a popup dialog with links and then navigating away from it when clicking on a link is a good usability practice. The question becomes, should the popup stay open, or should it close when reference link is clicked? I think better approach would’ve been to load links in the same page, and then open link in a new tab when clicked
   * Another approach could be splitting screen vertically and loading terms in a left panel, and when term is clicked loading related links in a right panel. Then click on a link would open a  new tab
* As I mentioned above, there could be a lot of patients for any term. Real time calculations of such data will slow down page load. I would recommend having some nightly job that calculates totals at regular intervals and populates denormalized table, so then it can be queried real time and data could be easily displayed
* Sorting and display of terms is another consideration. I did not add sorting to UI, but user might want to be able to sort by term alphabetically and/or by number of patients. I did add those to proposed service calls API
* Medical terms could have related terms, and there could be hierarchies of those. So the challenge would be to retrieve and display the tree of terms in an efficient manner.